package net.sf.l2j.gameserver.model.zone.type.subtype;

import net.sf.l2j.gameserver.model.actor.Player;

/**
 * A zone extending {@link SpawnZoneType} which handles following spawns type :
 * <ul>
 * <li>Generic spawn locs : owner_restart_point_list (spawns used on siege, to respawn on mass gatekeeper room.</li>
 * <li>Chaotic spawn locs : banish_point_list (spawns used to banish players on regular owner maintenance).</li>
 * </ul>
 */
public abstract class ResidenceZoneType extends SpawnZoneType
{
	private int _residenceId;
	
	public ResidenceZoneType(int id)
	{
		super(id);
	}
	
	public int getResidenceId()
	{
		return _residenceId;
	}
	
	protected void setResidenceId(int residenceId)
	{
		_residenceId = residenceId;
	}
	
	/**
	 * Kick {@link Player}s who don't belong to the clan set as parameter from this zone. They are ported to a random "chaotic" location.
	 * @param clanId : The castle owner clanId. Related players aren't teleported out.
	 */
	public void banishForeigners(int clanId)
	{
		for (Player player : getKnownTypeInside(Player.class))
		{
			if (player.getClanId() == clanId)
				continue;
			
			player.teleportTo(getBanishSpawnLoc(), 20);
		}
	}
}