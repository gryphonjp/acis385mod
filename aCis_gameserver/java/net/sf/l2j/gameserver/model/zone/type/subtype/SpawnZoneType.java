package net.sf.l2j.gameserver.model.zone.type.subtype;

import java.util.ArrayList;
import java.util.List;

import net.sf.l2j.commons.random.Rnd;

import net.sf.l2j.gameserver.model.location.Location;
import net.sf.l2j.gameserver.model.zone.ZoneType;

/**
 * An abstract zone with spawn locations, inheriting {@link ZoneType} behavior.<br>
 * <br>
 * Two lazy initialized {@link List}s can hold {@link Location}s.
 */
public abstract class SpawnZoneType extends ZoneType
{
	private List<Location> _spawnLocs = null;
	private List<Location> _otherSpawnLocs = null;
	private List<Location> _chaoticSpawnLocs = null;
	private List<Location> _banishSpawnLocs = null;
	
	public SpawnZoneType(int id)
	{
		super(id);
	}
	
	/**
	 * Add a {@link Location} to one of the Location containers. Initialize it if not yet initialized.
	 * @param loc : The Location to register.
	 * @param type : The type of container used to store the Location.
	 */
	public void parseLoc(Location loc, String type)
	{
		if (type == null || type.isEmpty())
			addSpawn(loc);
		else
		{
			switch (type)
			{
				case "other":
					addOtherSpawn(loc);
					break;
				
				case "chaotic":
					addChaoticSpawn(loc);
					break;
				
				case "banish":
					addBanishSpawn(loc);
					break;
				
				default:
					LOGGER.warn("Unknown location type {} encountered while parsing location.", type);
			}
		}
	}
	
	public final void addSpawn(Location loc)
	{
		if (_spawnLocs == null)
			_spawnLocs = new ArrayList<>();
		
		_spawnLocs.add(loc);
	}
	
	public final void addOtherSpawn(Location loc)
	{
		if (_otherSpawnLocs == null)
			_otherSpawnLocs = new ArrayList<>();
		
		_otherSpawnLocs.add(loc);
	}
	
	public final void addChaoticSpawn(Location loc)
	{
		if (_chaoticSpawnLocs == null)
			_chaoticSpawnLocs = new ArrayList<>();
		
		_chaoticSpawnLocs.add(loc);
	}
	
	public final void addBanishSpawn(Location loc)
	{
		if (_banishSpawnLocs == null)
			_banishSpawnLocs = new ArrayList<>();
		
		_banishSpawnLocs.add(loc);
	}
	
	public final List<Location> getSpawnLocs()
	{
		return _spawnLocs;
	}
	
	public final Location getSpawnLoc()
	{
		return Rnd.get(_spawnLocs);
	}
	
	public final Location getOtherSpawnLoc()
	{
		return (_otherSpawnLocs != null) ? Rnd.get(_otherSpawnLocs) : getSpawnLoc();
	}
	
	public final Location getChaoticSpawnLoc()
	{
		return (_chaoticSpawnLocs != null) ? Rnd.get(_chaoticSpawnLocs) : getSpawnLoc();
	}
	
	public final Location getBanishSpawnLoc()
	{
		return (_banishSpawnLocs != null) ? Rnd.get(_banishSpawnLocs) : getSpawnLoc();
	}
}