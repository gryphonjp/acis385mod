package net.sf.l2j.gameserver.model.zone.type;

import java.util.ArrayList;
import java.util.List;

import net.sf.l2j.gameserver.model.actor.Player;
import net.sf.l2j.gameserver.model.location.Location;

public final class SiegableHallZone extends ClanHallZone
{
	private List<Location> _challengerLocs = null;
	
	public SiegableHallZone(int id)
	{
		super(id);
	}
	
	@Override
	public void parseLoc(Location loc, String type)
	{
		if (type != null && type.equals("challenger"))
		{
			if (_challengerLocs == null)
				_challengerLocs = new ArrayList<>();
			
			_challengerLocs.add(loc);
		}
		else
			super.parseLoc(loc, type);
	}
	
	public List<Location> getChallengerLocs()
	{
		return _challengerLocs;
	}
	
	public void banishNonSiegeParticipants()
	{
		for (Player player : getKnownTypeInside(Player.class))
		{
			if (player.isInSieagableHallSiege())
				player.teleportTo(getBanishSpawnLoc(), 0);
		}
	}
}
