package com.elfocrash.roboto;

import java.util.List;
import java.util.stream.Collectors;

import com.elfocrash.roboto.helpers.FakeHelpers;

import net.sf.l2j.Config;
import net.sf.l2j.gameserver.data.xml.MapRegionData.TeleportType;
import net.sf.l2j.gameserver.data.manager.CastleManager;
import net.sf.l2j.gameserver.model.World;
import net.sf.l2j.gameserver.model.actor.Player;
import net.sf.l2j.gameserver.model.entity.Castle;
import net.sf.l2j.gameserver.model.entity.Siege;
import net.sf.l2j.gameserver.enums.SiegeSide;
import net.sf.l2j.gameserver.model.pledge.Clan;
import net.sf.l2j.gameserver.enums.ZoneId;
import net.sf.l2j.gameserver.network.SystemMessageId;
import net.sf.l2j.gameserver.network.serverpackets.PledgeShowMemberListUpdate;
import net.sf.l2j.gameserver.network.serverpackets.SystemMessage;

/**
 * @author Elfocrash
 *
 */
public enum FakePlayerManager {
	INSTANCE;

	private FakePlayerManager() {

	}

	public void initialise() {
		FakePlayerNameManager.INSTANCE.initialise();
		FakePlayerTaskManager.INSTANCE.initialise();
	}

	public FakePlayer spawnPlayer(int x, int y, int z) {
		FakePlayer activeChar = FakeHelpers.createRandomFakePlayer();
		World.getInstance().addPlayer(activeChar);
		handlePlayerClanOnSpawn(activeChar);
		
		if (Config.PLAYER_SPAWN_PROTECTION > 0)
			activeChar.setSpawnProtection(true);
		
		activeChar.spawnMe(x, y, z);
		activeChar.onPlayerEnter();
		
		/*if (!activeChar.isGM() && (!activeChar.isInSiege() || activeChar.getSiegeState() < 2)
				&& activeChar.isInsideZone(ZoneId.SIEGE))
			activeChar.teleToLocation(TeleportType.TOWN);*/

		activeChar.heal();
		return activeChar;
	}

	
	public FakePlayer spawnHealerPlayer(int x, int y, int z) {
		FakePlayer activeChar = FakeHelpers.createHealerFakePlayer();
		return this.spawnThisPlayer(x, y, z, activeChar);
	}
	public FakePlayer spawnArcherPlayer(int x, int y, int z) {
		FakePlayer activeChar = FakeHelpers.createRandomArcherFakePlayer();
		return this.spawnThisPlayer(x, y, z, activeChar);
	}
	public FakePlayer spawnMagePlayer(int x, int y, int z) {
		FakePlayer activeChar = FakeHelpers.createRandomMageFakePlayer();
		return this.spawnThisPlayer(x, y, z, activeChar);
	}
	public FakePlayer spawnMysticMuse(int x, int y, int z) {
		FakePlayer activeChar = FakeHelpers.createMysticMuseFakePlayer();
		return this.spawnThisPlayer(x, y, z, activeChar);
	}
	public FakePlayer spawnSoultaker(int x, int y, int z) {
		FakePlayer activeChar = FakeHelpers.createSoultakerFakePlayer();
		return this.spawnThisPlayer(x, y, z, activeChar);
	}
	public FakePlayer spawnArchmage(int x, int y, int z) {
		FakePlayer activeChar = FakeHelpers.createArchmageFakePlayer();
		return this.spawnThisPlayer(x, y, z, activeChar);
	}
	public FakePlayer spawnTanker(int x, int y, int z) {
		FakePlayer activeChar = FakeHelpers.createTankerFakePlayer();
		return this.spawnThisPlayer(x, y, z, activeChar);
	}


	private FakePlayer spawnThisPlayer(int x, int y, int z,FakePlayer activeChar){
		World.getInstance().addPlayer(activeChar);
		handlePlayerClanOnSpawn(activeChar);
		
		if (Config.PLAYER_SPAWN_PROTECTION > 0)
			activeChar.setSpawnProtection(true);
		
		activeChar.spawnMe(x, y, z);
		activeChar.onPlayerEnter();
		
		/*if (!activeChar.isGM() && (!activeChar.isInSiege() || activeChar.getSiegeState() < 2)
				&& activeChar.isInsideZone(ZoneId.SIEGE))
			activeChar.teleToLocation(TeleportType.TOWN);*/

		activeChar.heal();
		return activeChar;
	}

	
	public void despawnFakePlayer(int objectId) {
		Player player = World.getInstance().getPlayer(objectId);
		if (player instanceof FakePlayer) {
			FakePlayer fakePlayer = (FakePlayer) player;
			fakePlayer.despawnPlayer();
		}
	}

	private static void handlePlayerClanOnSpawn(FakePlayer activeChar) {
		final Clan clan = activeChar.getClan();
		if (clan != null) {
			clan.getClanMember(activeChar.getObjectId()).setPlayerInstance(activeChar);

			final SystemMessage msg = SystemMessage.getSystemMessage(SystemMessageId.CLAN_MEMBER_S1_LOGGED_IN)
					.addCharName(activeChar);
			final PledgeShowMemberListUpdate update = new PledgeShowMemberListUpdate(activeChar);

			// Send packets to others members.
			for (Player member : clan.getOnlineMembers()) {
				if (member == activeChar)
					continue;

				member.sendPacket(msg);
				member.sendPacket(update);
			}

			for (Castle castle : CastleManager.getInstance().getCastles()) {
				final Siege siege = castle.getSiege();
				if (!siege.isInProgress())
					continue;

				final SiegeSide type = siege.getSide(clan);
				if (type == SiegeSide.ATTACKER)
					activeChar.setSiegeState((byte) 1);
				else if (type == SiegeSide.DEFENDER || type == SiegeSide.OWNER)
					activeChar.setSiegeState((byte) 2);
			}
		}
	}

	public int getFakePlayersCount() {
		return getFakePlayers().size();
	}

	public List<FakePlayer> getFakePlayers() {
		return World.getInstance().getPlayers().stream().filter(x -> x instanceof FakePlayer).map(x -> (FakePlayer) x)
				.collect(Collectors.toList());
	}
}
